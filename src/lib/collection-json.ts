import * as Joi from 'joi';
import * as _ from 'lodash';
import { isURL } from 'validator';
const urljoin = require('url-join');


// 
// Interface definitions for
// collection-json elements
// 
interface IData {
    name: string;
    prompt?: string;
    value?: string;
}

interface IError {
    code: string;
    message: string;
    title: string;
}

interface ILink {
    href: string;
    rel: string;
    name?: string;
    prompt?: string;
    render?: string;
}

interface IItem {
    href: string;
    data?: IData[];
    links?: ILink[];
}

interface IQuery {
    href: string;
    rel: string;
    prompt: string;
    name: string;
    data: IData[];
}

interface ITemplate {
    data: IData[];
}

interface ICollectionJsonElements {
    version: string;
    href: string;
    links?: ILink[];
    items?: IItem[];
    queries?: IQuery[];
    template?: ITemplate;
    error?: IError;
}

// 
// Joi schema definitions
// 
const dataSchema = Joi.object().keys({
    // Requires the string value to only contain a-z, A-Z, 0-9, and underscore _.
    name: Joi.string().token().required(),
    prompt: Joi.string(),
    // Value is a string, number, boolean, null, object, or array.
    value: Joi.alternatives()
        .try(Joi.string().allow(null).allow(''), Joi.number(), Joi.boolean(), Joi.object(), Joi.array()),
});

const dataArraySchema = Joi.array().items(dataSchema);

const linkSchema = Joi.object().keys({
    href: Joi.string().required(),
    name: Joi.string().token(),
    prompt: Joi.string(),
    rel: Joi.string().required(),
    render: Joi.string().valid(['image', 'link']),
});

const linkArraySchema = Joi.array().items(linkSchema);

const itemSchema = Joi.object().keys({
    data: Joi.array().items(dataSchema),
    href: Joi.string().required(),
    links: Joi.array().items(linkSchema),
});

const itemsSchema = Joi.array().items(itemSchema);

const errorSchema = Joi.object().keys({
    code: Joi.string(),
    message: Joi.string(),
    title: Joi.string(),
});

const templateSchema = Joi.object().keys({
    data: dataArraySchema,
});

const querySchema = Joi.object().keys({
    data: dataArraySchema,
    href: Joi.string().required(),
    prompt: Joi.string(),
    rel: Joi.string().required(),
});

const queryArraySchema = Joi.array().items(querySchema);

// 
// THE ACTUAL EXPORTED CLASS
// 

// TODO: make validation optional?
export class CollectionJson {
    public collection: ICollectionJsonElements;

    constructor(url: string) {
        const options = {
            protocols: ['http', 'https'],
            require_host: true,
            require_protocol: true,
        };

        if (!isURL(url, options)) {
            throw new Error('Invalid url: "' + url + '". Cannot create cj-object.');
        };

        this.collection = {
            href: url,
            version: '1.0',
        };

    }

    // 
    // Items
    // 
    get items() {
        return this.collection.items;
    }

    set items(items: IItem[]) {
        const that = this;

        Joi.validate(items, itemsSchema, function (err, value) {
            // If array is invalid, check every item and return valid items
            if (err) {
                console.error(err);

                // TODO: add an error with validating error for each invalid item

                const filteredItems: IItem[] = items.map(item => {
                    return Joi.validate(item, itemSchema, function (err, value) {
                        if (err) {
                            console.error(err);
                            return;
                        }
                        return item;
                    });
                });

                that.collection.items = _.compact(filteredItems);
                return;
            }

            that.collection.items = items;
        });

    }


    public addItems(items: any[], options?: IRenderItemsOptions) {
        const url = this.collection.href;
        const result = renderItems(items, url, options);

        this.collection.items = result;
    }

    // 
    // Links
    // 
    set links(links: ILink[]) {

        const result = Joi.validate(links, linkArraySchema);

        if (result.error) {
            console.error(result.error);
            return;
        }

        this.collection.links = links;
    }

    get links() {

        return this.collection.links;

    }

    public addLinks(links: any[], options: IRenderLinksOptions) {
        const result = renderLinks(links, options);

        this.collection.links = result;
    }

    // 
    // Queries
    // 
    set queries(queries: IQuery[]) {
        const result = Joi.validate(queries, queryArraySchema);

        if (result.error) {
            console.error(result.error);
            return;
        }

        this.collection.queries = queries;
    }

    get queries() {

        return this.collection.queries;

    }

    public addQueries(queries: any[], options: IRenderQueriesOptions) {
        const result = renderQueries(queries, options);
        this.collection.queries = result;
    }

    // 
    // Template
    // 
    set template(template: ITemplate) {
        const result = Joi.validate(template, templateSchema);

        if (result.error) {
            console.error(result.error);
            return;
        }

        this.collection.template = template;
    }

    get template() {

        if (this.collection.template) {
            return this.collection.template;
        } else {
            return null;
        }
    }

    // 
    // Error
    // 
    set error(error: IError) {
        const result = Joi.validate(error, errorSchema);

        if (result.error) {
            console.error(result.error);
            return;
        }

        this.collection.error = error;
    }



}

interface IRenderDataOptions {
    data?: string;
    prompt?: string;
    name?: string;
    value?: string;
}

interface IRenderLinksOptions {
    href?: string;
    links?: string;
    rel?: string;
    prompt?: string;
    name?: string;
    render?: string;
}

interface IRenderItemsOptions {
    href?: string;
    id?: string;
    data?: IRenderDataOptions;
    links?: IRenderLinksOptions;
}

interface IRenderQueriesOptions {
    href?: string;
    rel?: string;
    prompt?: string;
    name?: string;
    data?: IRenderDataOptions;
}

function renderItems(items: any[], url: string, options?: IRenderItemsOptions): IItem[] {

    const result = items.map(item => {
        const cjItem: any = {};

        if (options.id) {
            cjItem.href = urljoin(url, item[options.id]);
        } else if (options.href) {
            cjItem.href = item[options.href];
        }

        let data: any[];
        let links: any[];

        if (options.data && options.data.data) {
            data = item[options.data.data];
        } else if (item.data) {
            data = item.data;
        }

        if (options.links && options.links.links) {
            links = item[options.links.links];
        } else if (item.links) {
            links = item.links;
        }

        // Proces data
        if (data && options.data) {

            cjItem.data = renderData(data, options.data);

        } else if (item.data) {

            Joi.validate(item.data, dataArraySchema, function (err, value) {
                if (err) {
                    console.error(err);

                    const data = item.data.map((d: IData) => {
                        const result = Joi.validate(d, dataSchema);
                        if (_.isNull(result.error)) {
                            return d;
                        };
                        console.error(result.error);
                    });

                    cjItem.data = data;
                    return;
                }

                cjItem.data = item.data;

            });
        }

        // Proces links
        if (links && options.links) {

            cjItem.links = renderLinks(links, options.links);

        } else if (item.links) {

            Joi.validate(item.links, linkArraySchema, function (err, value) {
                if (err) {
                    console.error(err);
                    return;
                }

                cjItem.links = item.links;

            });
        }

        return cjItem;
    });

    return Joi.validate(result, itemsSchema, function (err, value) {
        if (err) {
            // console.error(err);
            return;
        }

        return result;
    });
}

function renderData(data: any[], options: IRenderDataOptions): IData[] {
    let result: any[];

    result = data.map(d => {
        const r: any = {};

        if (options.name) {
            const name = options.name;
            r.name = d[name];
        } else {
            r.name = d.name;
        }

        if (options.value) {
            const value = options.value;
            r.value = d[value];
        } else if ((d.value) || _.isNull(d.value)) {
            r.value = d.value;
        }

        if (options.prompt) {
            const prompt = options.prompt;
            r.prompt = d[prompt];
        } else if (d.prompt) {
            r.prompt = d.prompt;
        }

        return Joi.validate(r, dataSchema, function (err, value) {
            if (err) {
                console.error(err);
                return;
            }
            return r;
        });

    });

    return result;
}

function renderLinks(links: any[], options: IRenderLinksOptions): ILink[] {
    let result: any[];

    result = links.map(link => {
        const renderedLink: any = {};

        // REQUIRED PROPERTIE
        if (options.href) {
            renderedLink.href = link[options.href];
        } else {
            renderedLink.href = link.href;
        }

        // REQUIRED PROPERTIE
        if (options.rel) {
            renderedLink.rel = link[options.rel];
        } else {
            renderedLink.rel = link.rel;
        }

        // OPTIONAL PROPERTY
        if (options.name) {
            renderedLink.name = link[options.name];
        } else if (link.name) {
            renderedLink.name = link.name;
        }

        // OPTIONAL PROPERTY
        if (options.prompt) {
            renderedLink.prompt = link[options.prompt];
        } else if (link.prompt) {
            renderedLink.prompt = link.prompt;
        }

        // OPTIONAL PROPERTY
        if (options.render) {
            renderedLink.render = link[options.render];
        } else if (link.render) {
            renderedLink.render = link.render;
        }

        const result = Joi.validate(renderedLink, linkSchema);

        if (result.error) {
            console.error(result.error);
            return;
        }

        return renderedLink;
    });

    return result;
}

function renderQueries(queries: any[], options: IRenderQueriesOptions): IQuery[] {
    let result: any[];

    // console.log('renderQueries', queries);

    result = queries.map(query => {
        const renderedQuery: any = {};

        let data: any[];

        if (options.data && options.data.data) {
            data = query[options.data.data];
        } else if (query.data) {
            data = query.data;
        }

        // Proces data
        if (data && options.data) {

            renderedQuery.data = renderData(data, options.data);

        } else if (query.data) {

            Joi.validate(query.data, dataArraySchema, function (err, value) {
                if (err) {
                    console.error(err);

                    const data = query.data.map((d: IData) => {
                        const result = Joi.validate(d, dataSchema);
                        if (_.isNull(result.error)) {
                            return d;
                        };
                        console.error(result.error);
                    });

                    renderedQuery.data = data;
                    return;
                }

                renderedQuery.data = query.data;

            });
        }


        // REQUIRED PROPERTIE
        if (options.href) {
            renderedQuery.href = query[options.href];
        } else {
            renderedQuery.href = query.href;
        }

        // REQUIRED PROPERTIE
        if (options.rel) {
            renderedQuery.rel = query[options.rel];
        } else {
            renderedQuery.rel = query.rel;
        }

        // OPTIONAL PROPERTY
        if (options.name) {
            renderedQuery.name = query[options.name];
        } else if (query.name) {
            renderedQuery.name = query.name;
        }

        // OPTIONAL PROPERTY
        if (options.prompt) {
            renderedQuery.prompt = query[options.prompt];
        } else if (query.prompt) {
            renderedQuery.prompt = query.prompt;
        }

        const result = Joi.validate(renderedQuery, querySchema);

        if (result.error) {
            console.error(result.error);
            return;
        }

        return renderedQuery;
    });

    return result;
}