"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Joi = require("joi");
const _ = require("lodash");
const validator_1 = require("validator");
const urljoin = require('url-join');
const dataSchema = Joi.object().keys({
    name: Joi.string().token().required(),
    prompt: Joi.string(),
    value: Joi.alternatives()
        .try(Joi.string().allow(null).allow(''), Joi.number(), Joi.boolean(), Joi.object(), Joi.array()),
});
const dataArraySchema = Joi.array().items(dataSchema);
const linkSchema = Joi.object().keys({
    href: Joi.string().required(),
    name: Joi.string().token(),
    prompt: Joi.string(),
    rel: Joi.string().required(),
    render: Joi.string().valid(['image', 'link']),
});
const linkArraySchema = Joi.array().items(linkSchema);
const itemSchema = Joi.object().keys({
    data: Joi.array().items(dataSchema),
    href: Joi.string().required(),
    links: Joi.array().items(linkSchema),
});
const itemsSchema = Joi.array().items(itemSchema);
const errorSchema = Joi.object().keys({
    code: Joi.string(),
    message: Joi.string(),
    title: Joi.string(),
});
const templateSchema = Joi.object().keys({
    data: dataArraySchema,
});
const querySchema = Joi.object().keys({
    data: dataArraySchema,
    href: Joi.string().required(),
    prompt: Joi.string(),
    rel: Joi.string().required(),
});
const queryArraySchema = Joi.array().items(querySchema);
class CollectionJson {
    constructor(url) {
        const options = {
            protocols: ['http', 'https'],
            require_host: true,
            require_protocol: true,
        };
        if (!validator_1.isURL(url, options)) {
            throw new Error('Invalid url: "' + url + '". Cannot create cj-object.');
        }
        ;
        this.collection = {
            href: url,
            version: '1.0',
        };
    }
    get items() {
        return this.collection.items;
    }
    set items(items) {
        const that = this;
        Joi.validate(items, itemsSchema, function (err, value) {
            if (err) {
                console.error(err);
                const filteredItems = items.map(item => {
                    return Joi.validate(item, itemSchema, function (err, value) {
                        if (err) {
                            console.error(err);
                            return;
                        }
                        return item;
                    });
                });
                that.collection.items = _.compact(filteredItems);
                return;
            }
            that.collection.items = items;
        });
    }
    addItems(items, options) {
        const url = this.collection.href;
        const result = renderItems(items, url, options);
        this.collection.items = result;
    }
    set links(links) {
        const result = Joi.validate(links, linkArraySchema);
        if (result.error) {
            console.error(result.error);
            return;
        }
        this.collection.links = links;
    }
    get links() {
        return this.collection.links;
    }
    addLinks(links, options) {
        const result = renderLinks(links, options);
        this.collection.links = result;
    }
    set queries(queries) {
        const result = Joi.validate(queries, queryArraySchema);
        if (result.error) {
            console.error(result.error);
            return;
        }
        this.collection.queries = queries;
    }
    get queries() {
        return this.collection.queries;
    }
    addQueries(queries, options) {
        const result = renderQueries(queries, options);
        this.collection.queries = result;
    }
    set template(template) {
        const result = Joi.validate(template, templateSchema);
        if (result.error) {
            console.error(result.error);
            return;
        }
        this.collection.template = template;
    }
    get template() {
        if (this.collection.template) {
            return this.collection.template;
        }
        else {
            return null;
        }
    }
    set error(error) {
        const result = Joi.validate(error, errorSchema);
        if (result.error) {
            console.error(result.error);
            return;
        }
        this.collection.error = error;
    }
}
exports.CollectionJson = CollectionJson;
function renderItems(items, url, options) {
    const result = items.map(item => {
        const cjItem = {};
        if (options.id) {
            cjItem.href = urljoin(url, item[options.id]);
        }
        else if (options.href) {
            cjItem.href = item[options.href];
        }
        let data;
        let links;
        if (options.data && options.data.data) {
            data = item[options.data.data];
        }
        else if (item.data) {
            data = item.data;
        }
        if (options.links && options.links.links) {
            links = item[options.links.links];
        }
        else if (item.links) {
            links = item.links;
        }
        if (data && options.data) {
            cjItem.data = renderData(data, options.data);
        }
        else if (item.data) {
            Joi.validate(item.data, dataArraySchema, function (err, value) {
                if (err) {
                    console.error(err);
                    const data = item.data.map((d) => {
                        const result = Joi.validate(d, dataSchema);
                        if (_.isNull(result.error)) {
                            return d;
                        }
                        ;
                        console.error(result.error);
                    });
                    cjItem.data = data;
                    return;
                }
                cjItem.data = item.data;
            });
        }
        if (links && options.links) {
            cjItem.links = renderLinks(links, options.links);
        }
        else if (item.links) {
            Joi.validate(item.links, linkArraySchema, function (err, value) {
                if (err) {
                    console.error(err);
                    return;
                }
                cjItem.links = item.links;
            });
        }
        return cjItem;
    });
    return Joi.validate(result, itemsSchema, function (err, value) {
        if (err) {
            return;
        }
        return result;
    });
}
function renderData(data, options) {
    let result;
    result = data.map(d => {
        const r = {};
        if (options.name) {
            const name = options.name;
            r.name = d[name];
        }
        else {
            r.name = d.name;
        }
        if (options.value) {
            const value = options.value;
            r.value = d[value];
        }
        else if ((d.value) || _.isNull(d.value)) {
            r.value = d.value;
        }
        if (options.prompt) {
            const prompt = options.prompt;
            r.prompt = d[prompt];
        }
        else if (d.prompt) {
            r.prompt = d.prompt;
        }
        return Joi.validate(r, dataSchema, function (err, value) {
            if (err) {
                console.error(err);
                return;
            }
            return r;
        });
    });
    return result;
}
function renderLinks(links, options) {
    let result;
    result = links.map(link => {
        const renderedLink = {};
        if (options.href) {
            renderedLink.href = link[options.href];
        }
        else {
            renderedLink.href = link.href;
        }
        if (options.rel) {
            renderedLink.rel = link[options.rel];
        }
        else {
            renderedLink.rel = link.rel;
        }
        if (options.name) {
            renderedLink.name = link[options.name];
        }
        else if (link.name) {
            renderedLink.name = link.name;
        }
        if (options.prompt) {
            renderedLink.prompt = link[options.prompt];
        }
        else if (link.prompt) {
            renderedLink.prompt = link.prompt;
        }
        if (options.render) {
            renderedLink.render = link[options.render];
        }
        else if (link.render) {
            renderedLink.render = link.render;
        }
        const result = Joi.validate(renderedLink, linkSchema);
        if (result.error) {
            console.error(result.error);
            return;
        }
        return renderedLink;
    });
    return result;
}
function renderQueries(queries, options) {
    let result;
    result = queries.map(query => {
        const renderedQuery = {};
        let data;
        if (options.data && options.data.data) {
            data = query[options.data.data];
        }
        else if (query.data) {
            data = query.data;
        }
        if (data && options.data) {
            renderedQuery.data = renderData(data, options.data);
        }
        else if (query.data) {
            Joi.validate(query.data, dataArraySchema, function (err, value) {
                if (err) {
                    console.error(err);
                    const data = query.data.map((d) => {
                        const result = Joi.validate(d, dataSchema);
                        if (_.isNull(result.error)) {
                            return d;
                        }
                        ;
                        console.error(result.error);
                    });
                    renderedQuery.data = data;
                    return;
                }
                renderedQuery.data = query.data;
            });
        }
        if (options.href) {
            renderedQuery.href = query[options.href];
        }
        else {
            renderedQuery.href = query.href;
        }
        if (options.rel) {
            renderedQuery.rel = query[options.rel];
        }
        else {
            renderedQuery.rel = query.rel;
        }
        if (options.name) {
            renderedQuery.name = query[options.name];
        }
        else if (query.name) {
            renderedQuery.name = query.name;
        }
        if (options.prompt) {
            renderedQuery.prompt = query[options.prompt];
        }
        else if (query.prompt) {
            renderedQuery.prompt = query.prompt;
        }
        const result = Joi.validate(renderedQuery, querySchema);
        if (result.error) {
            console.error(result.error);
            return;
        }
        return renderedQuery;
    });
    return result;
}
