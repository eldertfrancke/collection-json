"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const assert = require('assert');
const index_1 = require("../index");
describe('new collection', () => {
    it('should create a collection-json object with a href', () => {
        const baseUrl = 'http://localhost/';
        const cjson = new index_1.CollectionJson(baseUrl);
        assert.equal(baseUrl, cjson.collection.href);
    });
});
describe('new collection', () => {
    it('should throw invalid url', () => {
        const baseUrl = 'httq://localhost/';
        assert.throws(() => {
            new index_1.CollectionJson(baseUrl);
        }, /invalid url/i);
    });
});
describe('set items', () => {
    it('should add a set of items', () => {
        const urlString = 'http://localhost/';
        const cjson = new index_1.CollectionJson(urlString);
        const date = new Date().toISOString();
        const items = [
            { href: 'http://localhost/1/', data: [{ name: 'date date', value: date }, { name: 'klant', value: 'A' }], links: [{ href: 'http://localhost/2', rel: 'next' }] },
            { href: 'http://localhost/2/', data: [{ name: 'date', value: date }, { name: 'klant', value: 'B' }] },
            { href: 'http://localhost/3/', data: [{ name: 'date', value: date }, { name: 'klant', value: 'C' }] },
        ];
        cjson.items = items;
        assert.equal(cjson.collection.items.length, items.length - 1);
        assert.deepEqual(items[1], cjson.collection.items[0]);
    });
});
describe('set items', () => {
    it('should push an item', () => {
        const urlString = 'http://localhost/';
        const cjson = new index_1.CollectionJson(urlString);
        const date = new Date().toISOString();
        const items = [
            { href: 'http://localhost/1/', data: [{ name: 'date', value: date }, { name: 'klant', value: 'A' }], links: [{ href: 'http://localhost/2', rel: 'next' }] },
            { href: 'http://localhost/2/', data: [{ name: 'date', value: date }, { name: 'klant', value: 'B' }] },
            { href: 'http://localhost/3/', data: [{ name: 'date', value: date }, { name: 'klant', value: 'C' }] },
        ];
        const extraItem = { href: 'http://localhost/4/', data: [{ name: 'date', value: date }, { name: 'klant', value: 'D' }] };
        cjson.items = items;
        cjson.items.push(extraItem);
        assert.equal(cjson.items.length, 4);
        items.push(extraItem);
        assert.deepEqual(cjson.items, items);
    });
});
describe('add items', () => {
    it('should add a set of items', () => {
        const urlString = 'http://localhost/api/';
        const cjson = new index_1.CollectionJson(urlString);
        const date = new Date().toISOString();
        const items = [
            { uid: '1', props: [{ name: 'date', value: date }, { name: 'klant', value: 'A' }] },
            { uid: '2', props: [{ name: 'date', value: date }, { name: 'klant', value: 'B' }] },
            { uid: '3', props: [{ name: 'date', value: null }, { name: 'klant', value: 'C' }] },
        ];
        const cfItems = [
            { href: 'http://localhost/api/1', data: [{ name: 'date', value: date, prompt: 'date' }, { name: 'klant', value: 'A', prompt: 'klant' }] },
            { href: 'http://localhost/api/2', data: [{ name: 'date', value: date, prompt: 'date' }, { name: 'klant', value: 'B', prompt: 'klant' }] },
            { href: 'http://localhost/api/3', data: [{ name: 'date', value: null, prompt: 'date' }, { name: 'klant', value: 'C', prompt: 'klant' }] },
        ];
        const options = {
            data: {
                data: 'props',
                prompt: 'name',
            },
            id: 'uid',
        };
        cjson.addItems(items, options);
        assert.deepEqual(cjson.collection.items, cfItems);
    });
});
describe('addLinks', () => {
    it('should render the links correctly', () => {
        const urlString = 'http://localhost/api/';
        const cjson = new index_1.CollectionJson(urlString);
        const links = [
            { type: 'feed', href: 'http://localhost/rss' },
        ];
        const cfLinks = [
            { rel: 'feed', href: 'http://localhost/rss' },
        ];
        const options = {
            rel: 'type',
        };
        cjson.addLinks(links, options);
        assert.deepEqual(cjson.collection.links, cfLinks);
    });
});
describe('addQueries', () => {
    it('should render the queries correctly', () => {
        const urlString = 'http://localhost/api/';
        const cjson = new index_1.CollectionJson(urlString);
        const queries = [
            {
                data: [
                    { name: 'name', value: '' },
                ],
                prompt: 'Search',
                type: 'search',
                url: 'http://localhost/search',
            },
        ];
        const cfQueries = [
            {
                data: [
                    { name: 'name', value: '' },
                ],
                href: 'http://localhost/search',
                prompt: 'Search',
                rel: 'search',
            },
        ];
        const options = {
            href: 'url',
            rel: 'type',
        };
        cjson.addQueries(queries, options);
        assert.deepEqual(cjson.collection.queries, cfQueries);
    });
});
describe('set template', () => {
    it('should add a valid cj-template', () => {
        const urlString = 'http://localhost/api/';
        const cjson = new index_1.CollectionJson(urlString);
        const template = {
            data: [
                { name: 'serialnumber', prompt: 'Serienummer' },
            ],
        };
        cjson.template = template;
        assert.deepEqual(cjson.collection.template, template);
    });
});
describe('set error', () => {
    it('should add a valid error', () => {
        const urlString = 'http://localhost/api/';
        const cjson = new index_1.CollectionJson(urlString);
        const error = {
            code: 'E9999',
            message: 'Something went wrong.',
            title: 'Error',
        };
        cjson.error = error;
        assert.deepEqual(cjson.collection.error, error);
    });
});
