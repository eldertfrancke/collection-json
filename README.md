# Create and validate a collection+json object
[Collection+JSON](https://github.com/collection-json/spec) is a JSON-based
read/write hypermedia-type designed to support management and querying of
simple collections.

With this library you can create a collection+json object and validate your input.

## Installation
```
npm i yacoson
```

## Examples
### Minimal representation
```
const CollectionJson = require('yacoson').CollectionJson;

let cj = new CollectionJson('http://localhost/api'); 

console.log(JSON.stringify(cj, null, '  ')); 
// { "collection" : 
//   {
//     "version" : "1.0",
//     "href" : "http://localhost/api"
//   } 
// }
```

An error is thrown in case the url is invalid.

### Item respresentation

You can assign items to the items property.
It only adds valid property items. Invalid ones are filtered out.

```
cj.items = [
    { 
        href: 'http://localhost/api/1/',
        data: [
            { name: 'date', value: "2017-02-22T09:39:34.860Z" },
            { name: 'client', value: 'xyz' },
        ] 
    },
];

console.log(JSON.stringify(cj, null, '  ')); 
// {
//   "collection": {
//     "href": "http://localhost/api",
//     "version": "1.0",
//     "items": [
//       {
//         "href": "http://localhost/api/1/",
//         "data": [
//           {
//             "name": "date",
//             "value": "2017-02-22T09:39:34.860Z"
//           },
//           {
//             "name": "client",
//             "value": "xyz"
//           }
//         ]
//       }
//     ]
//   }
// }
```

```

cj.addItems(items, options);
```
## API
Creating a new object requires a string with a valid url, if not an error is thrown.

### Properties
These properties are exposed to assign and validate your data:
 - items, cj.items = item[];
 - links, cj.links = link[];
 - queries, cj.queries = query[];
 - template, cj.template = template;
 - error, cj.error = error;

Besides this you can of course assign your data to the collection property to bypass validation.

### Methods
#### addItems

```
const options = {
    href: string, // name of property with prompt value
    id: string,  // name of property with id value used to build href, overrides href
    data:  {
        data: string,  // name of property with data array
        prompt: string, // name of property with prompt value
        name: string, // name of property with name value
        value: string,  // name of property with prompt value
    },
    links: {
        href: string,  // name of property with href value
        links: string,   // name of property with links array
        rel: string,   // name of property with rel value
        prompt: string,  // name of property with prompt value
        name: string,  // name of property with name value
        render: string,  // name of property with render value
    },
}

cj.addItems(item[], options)
```
Example
```
const items = [
            { uid: '1', props: [{ name: 'date', value: date }, { name: 'klant', value: 'A' }] },
            { uid: '2', props: [{ name: 'date', value: date }, { name: 'klant', value: 'B' }] },
            { uid: '3', props: [{ name: 'date', value: date }, { name: 'klant', value: 'C' }] },
];

const options = {
            data: {
                data: 'props',
                prompt: 'name',
            },
            id: 'uid',
};

cj.addItems(items, options)
```

#### addLinks
```
let options = {
    href: string,   // name of property with href value
    rel: string,  // name of property with rel value
    prompt: string,  // name of property with prompt value
    name: string,  // name of property with name value
    render: string,  // name of property with render value
}

addLinks(link[], options)
```
#### addQueries
```
let options = {
    href: string,   // name of property with href value
    rel: string,  // name of property with rel value
    prompt: string,  // name of property with prompt value
    name: string,  // name of property with name value
    data:  {
        data: string,  // name of property with data array
        prompt: string, // name of property with prompt value
        name: string, // name of property with name value
        value: string,  // name of property with prompt value
    },
}

addQueries(query[], options)
```
## Validation
### Name
The collection-json specification states the following about the name type:
> It SHOULD be a STRING data type.

Our validation rule is a bit more restrictive:
> The name property SHOULD be a STRING data type and only contain a-z, A-Z, 0-9, and underscore _.